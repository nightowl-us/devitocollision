﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bluedropzone : MonoBehaviour
{
    WhatTouchedMe whattouchedme;
    PointGuard pointguard;
    HowmuchDoIHave howmuch;
    public GameObject Garbage;
    public GameObject God;
    public GameObject Hatchblue;

    public void Start()
    {
        
        Garbage = GameObject.Find("actualtrash");
        God = GameObject.Find("God");
        Hatchblue = GameObject.Find("HatchedBackBlue");
        pointguard = God.GetComponent<PointGuard>();
        whattouchedme = Garbage.GetComponent<WhatTouchedMe>();
        howmuch = Hatchblue.GetComponent<HowmuchDoIHave>();
    } 
    
      void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "blue")
        {

            pointguard.BluePoints += howmuch.CurrentTrash;
            howmuch.CurrentTrash = 0;
            pointguard.grantPointToBlueTeam(true);

            Debug.Log(pointguard.BluePoints);
        }
    }

}
