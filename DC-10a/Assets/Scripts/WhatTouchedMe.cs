﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhatTouchedMe : MonoBehaviour
{
    public GameObject TrashTruck;
    public GameObject Hearse;
    public GameObject Garbage;

    public bool hasBeenUsed = false;

    public bool BlueTouched = false;
    public bool RedTouched = false;

    /*public bool TouchedMe;
    public bool TouchedMe;
    /*public GameObject BlueTrashTruck;
    public GameObject RedTrashTruck;*/
    /*public GameObject BlueHearse;
    public GameObject RedHearse;*/


    void Start()
    {
        Garbage = GameObject.Find("actualTrash");
        
        /*Hearse = GameObject.Find("HatchedBack");
        TrashTruck = GameObject.Find("TrashTruck");*/
    }

    void OnCollisionEnter(Collision other)
    {
        if (!hasBeenUsed)
        {
            if (other.gameObject.tag == "blue")

            {
                hasBeenUsed = true;
                
                //BlueTouched = true;
                //GameObject.Find("God").GetComponent<PointGuard>().grantPointToBlueTeam(true);
            }

            //hasBeenUsed = false;
            //TTruckTouched = false;

            if (other.gameObject.tag == "red")

            {
                hasBeenUsed = true;

                //RedTouched = true;
                //GameObject.Find("God").GetComponent<PointGuard>().grantPointToRedTeam(true);
            }
        }
    }

    void FixedUpdate()
    {
        if (hasBeenUsed)
        {
            Destroy(this.gameObject);
        }
        hasBeenUsed = false;
    }
}