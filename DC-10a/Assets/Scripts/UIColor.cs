﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIColor : MonoBehaviour {
    public Color team1 = new Color(1.0F, 0.0F, 0.0F, 0.65F);
    public Color team2 = new Color(0.0F, 0.0F, 1.0F, 0.65F);
    // Use this for initialization
    void Start () {
        
    Image img = GameObject.Find("TeamColor").GetComponent<Image>();
    int team = PlayerPrefs.GetInt("PlayerTeam");
        if (team == 1)
        {
            img.color = team1;
        }
        else if (team == 2)
        {
            img.color = team2;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
