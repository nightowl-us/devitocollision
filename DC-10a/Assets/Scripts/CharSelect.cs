﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CharSelect : MonoBehaviour
{
    private GameObject[] characterList;
    private GameObject listCharacters;
    private int index;

    private int playerID()
    {
        if (listCharacters.tag == "Player1")
        {
            return 0;
        }
        else if (listCharacters.tag == "Player2")
        {
            return 1;
        }
        else
        {
            return 404;
        }
    }

    private void Start()
    {
        index = PlayerPrefs.GetInt("CharacterSelected");

        characterList = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            characterList[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject go in characterList)
        {
            go.SetActive(false);
        }
        if(characterList[index])
        {
            characterList[index].SetActive(true);
        }

       
    }
    
    public void ToggleLeft()
    {
        // toggle off current model
        characterList[index].SetActive(false);

        index--;
        if (index < 0)
        {
            index = characterList.Length - 1;
        }
        characterList[index].SetActive(true);
    }

    public void ToggleRight()
    {
        // toggle off current model
        characterList[index].SetActive(false);

        index++;
        if (index == characterList.Length)
        {
            index = 0;
        }
        characterList[index].SetActive(true);
    }

    public void ConfirmButton()
    {
        
        PlayerPrefs.SetInt("CharacterSelected", index);
        SceneManager.LoadScene("JohnnyTestReal");
    }

    public void SelectTeam1()
    {
        PlayerPrefs.SetInt("PlayerTeam", 1);
        //team red
    }

    public void SelectTeam2()
    {
        PlayerPrefs.SetInt("PlayerTeam", 2);
        //team blue
    }
    public void WhoAmI()
    {
        PlayerPrefs.SetInt("ID", playerID());
        //sets value "ID" in player preferences to keep track of which player is which
    }
}
