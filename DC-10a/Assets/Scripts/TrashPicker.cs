﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashPicker : MonoBehaviour
{
    public GameObject Garbage;

    public bool Touched = false;
    void Start()
    {

        Garbage = GameObject.Find("GarbageBag");
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Garbage")
        {
            Touched = true;
            
        }
        //Touched = false;
    }
}
